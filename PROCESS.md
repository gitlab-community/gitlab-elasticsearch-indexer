# GitLab Elasticsearch Indexer development process

## Maintainers

GitLab Elasticsearch Indexer has the following maintainers:

- Dylan Griffith `@DylanGriffith`
- Dmitry Gruzd `@dgruzd`
- Terri Chu `@terrichu`
- John Mason `@johnmason`

This list is defined at https://about.gitlab.com/team/

### How to become a maintainer

GitLab Elasticsearch Indexer follows a maintainership process based on the [smaller project
template](https://about.gitlab.com/handbook/engineering/workflow/code-review/#maintainership-process-for-smaller-projects). 

Anyone may nominate themselves as a trainee by opening a tracking issue using the [`gitlab-elasticsearch-indexer` trainee maintainer template](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/new?issuable_template=trainee-gitlab-elasticsearch-indexer-maintainer&issue[title]=gitlab-elasticsearch-indexer%20Trainee%20Maintainer%3A%20%5BFull%20Name%5D). It's normally a good idea to check with at least one maintainer or your manager before creating the issue, but it's not required.

## Merging and reviewing contributions

Contributions must be reviewed by at least one maintainer. The final merge must
be performed by a maintainer.

## Releases

> **WARNING**: Tags are mirrored on the [security repository](https://gitlab.com/gitlab-org/security/gitlab-elasticsearch-indexer/-/tags) and [dev repository](https://dev.gitlab.org/gitlab/gitlab-elasticsearch-indexer/-/tags).
Deleting tags does not get updated on mirroring, so any deleted tag must be 
manually deleted on the mirrored repositories.

We use [semantic-release](https://semantic-release.gitbook.io/semantic-release/)
to generate changelog entries, release commits, bump the `VERSION` file, and create new git tags. 

A new release is created by the project maintainers, using the `make release` command,
invoked from their local development machine. 

A `make release-dry-run` command is available to anyone and allows previewing the next release.
If this is the first time you are generating a release, you must invoke the
`make release-tools` command to install the required dependencies. This requires
having Node.js and npm installed locally.

Once a new tag is released, open a merge request to `gitlab-org/gitlab` to update the
[version in `GITLAB_ELASTICSEARCH_INDEXER_VERSION`](https://gitlab.com/gitlab-org/gitlab/-/blob/master/GITLAB_ELASTICSEARCH_INDEXER_VERSION).

## Versioning

GitLab Elasticsearch Indexer uses a variation of SemVer. We don't use
"normal" SemVer because we have to be able to integrate into GitLab stable
branches.

A version has the format MAJOR.MINOR.PATCH.

- Major and minor releases are tagged on the `main` branch
- If the change is backwards compatible, increment the MINOR counter
- If the change breaks compatibility, increment MAJOR and set MINOR to `0`
- Patch release tags must be made on stable branches
- Only make a patch release when targeting a GitLab stable branch

This means that tags that end in `.0` (e.g. `8.5.0`) must always be on
the main branch, and tags that end in anything other than `.0` (e.g.
`8.5.2`) must always be on a stable branch.

> The reason we do this is that SemVer suggests something like a
> refactoring constitutes a "patch release", while the GitLab stable
> branch quality standards do not allow for back-porting refactorings
> into a stable branch.
