# GitLab Elasticsearch Indexer

This project indexes Git repositories into Elasticsearch for GitLab. See the
[homepage](https://gitlab.com/gitlab-org/gitlab-elasticsearch-indexer) for more
information.

## Dependencies

This project relies on [ICU](http://site.icu-project.org/) for text encoding;
ensure the development packages for your platform are installed before running
`make`:

### Debian / Ubuntu

```
# apt install libicu-dev
```

### Mac OSX

```
$ brew install icu4c
$ export PKG_CONFIG_PATH="$(brew --prefix)/opt/icu4c/lib/pkgconfig:$PKG_CONFIG_PATH"
```

## Building & Using Docker Image

```
docker build . -t gitlab-elasticsearch-indexer
```

You can edit your shell profile (like `~/.zshrc`) to use the image as a binary.
```
func gitlab-elasticsearch-indexer() {
  docker run --rm -it gitlab-elasticsearch-indexer "$@"
}
```

## Building & Installing

```
make
sudo make install
```

`gitlab-elasticsearch-indexer` will be installed to `/usr/local/bin`

You can change the installation path with the `PREFIX` env variable. Please remember to pass the `-E` flag to sudo if you do so.

Example:
```
PREFIX=/usr sudo -E make install
```

## Lefthook static analysis

[Lefthook](https://github.com/evilmartians/lefthook) is a Git hooks manager that allows
custom logic to be executed prior to Git committing or pushing. `gitlab-elasticsearch-indexer`
comes with Lefthook configuration (`lefthook.yml`), but it must be installed.

We have a `lefthook.yml` checked in but it is ignored until Lefthook is installed.

### Install Lefthook

1. [Install `lefthook`](https://github.com/evilmartians/lefthook/blob/master/docs/full_guide.md#installation)
1. Install Lefthook Git hooks:

   ```shell
   lefthook install
   ```

1. Test Lefthook is working by running the Lefthook `pre-push` Git hook:

   ```shell
   lefthook run pre-push
   ```

## Run tests

Test suite expects Gitaly and Elasticsearch to be run, on the following ports:

  - Gitaly: 8075
  - ElasticSearch v7.14.2: 9201

Make sure you have `docker` and `docker-compose` installed. You can use [colima](https://gitlab.com/-/snippets/2259133) to run docker since [Docker Desktop cannot be used due to licensing](https://about.gitlab.com/handbook/tools-and-tips/mac/#docker-desktop).

```bash
brew install docker docker-compose colima
colima start
````

### Quick tests

```bash
# you only have to run this once, as it starts the services. It might take few seconds to make the services up.
make test-infra

# source the default connections
source .env.test

# run the test suite
make test

# or run a specific test
go test -v gitlab.com/gitlab-org/gitlab-elasticsearch-indexer -run TestIndexingGitlabTest

```

If you want to re-create the infra, you can run `make test-infra` again.

### Custom tests

If you want to test a particular setup, for instance:

  - You want to run on a local Gitaly instance, as the one from the GDK
  - You want to use a specific ElasticSearch cluster, as the one from the GDK

Then you'll have to manually set the proper tests connections.

First, start the services that you need (`gitlab`, `elasticsearch`), with using `docker-compose up -d <service>`

```bash
# to start Gitaly
docker-compose up -d gitaly

# to start ElasticSearch
docker-compose up -d elasticsearch
```

Before running tests, set configuration variables

```bash
# these are the defaults, in `.env.test`

export GITALY_CONNECTION_INFO='{"address": "tcp://localhost:8075", "storage": "default"}'
export ELASTIC_CONNECTION_INFO='{"url":["http://localhost:9201"], "index_name":"gitlab-test", "index_name_commits":"gitlab-test-commits"}'
```

**Note**: If using a socket, please pass your URI in the form `unix://FULL_PATH_WITH_LEADING_SLASH`

Example:
```bash
# source the default connections
source .env.test

# override the Gitaly connection
export GITALY_CONNECTION_INFO='{"address": "unix:///gitlab/gdk/gitaly.socket", "storage": "default"}'

# run the test suite
make test

# or a specific test
go test -v gitlab.com/gitlab-org/gitlab-elasticsearch-indexer -run TestIndexingGitlabTest
```

## Testing in GDK

You can test changes to the index in GDK in multiple ways.

### `GITLAB_ELASTICSEARCH_INDEXER_VERSION` file

**Warning:** Do not use tags to test code. Tags are created for released versions only.

The [GITLAB_ELASTICSEARCH_INDEXER_VERSION file](https://gitlab.com/gitlab-org/gitlab/-/blob/master/GITLAB_ELASTICSEARCH_INDEXER_VERSION) accepts commit SHA and branch names. This method works for local development and spec execution.

### Build binary to gdk directory

You can test changes to the indexer in your GDK by building the `gitlab-elasticsearch-indexer` and using the `PREFIX` env variable to change the installation directory to the gdk directory. Running `gdk update` will reset the `gitlab-elasticsearch-indexer` back to the current supported version. This will not affect specs, the specs use the `GITLAB_ELASTICSEARCH_INDEXER_VERSION` to build the indexer to `<gdk_install_directory>/gitlab/tmp/tests/gitlab-elasticsearch-indexer`. Any code changes require re-building the indexer to test in GDK.

Example:
```bash
PREFIX=<gdk_install_directory>/gitlab-elasticsearch-indexer make install
```

### Debugging tests with delve

You can use [delve](https://github.com/go-delve/delve/) to debug the unit tests, add breakpoints, print values etc.

You can you the following command to enter `dlv` terminal:

```bash
dlv test <path-to-package> -- -test.run <regex-matching-test-name>
```

For Example:
```bash
dlv test gitlab.com/gitlab-org/gitlab-elasticsearch-indexer -- -test.run ^TestIndexingWikiBlobs$
```

After you enter the terminal, you can use the following commands for debugging:
 - Setting a break point `break <path-to-file>:<line-number>`
 - Continue till break point `continue`
 - Printing a value `print <variable-name>`
 - Step over to next source line `next`
 - Exit the environment `exit`

For more information, you can go through [this documentation](https://github.com/go-delve/delve/blob/master/Documentation/cli/getting_started.md#debugging-tests).

## Obtaining a package or Docker image for testing an MR

GitLab team members can use the `build-package-and-qa` job in their MR pipeline
to trigger a pipeline in the `omnibus-gitlab-mirror` project. This pipeline
produces an `omnibus-gitlab` package for Ubuntu (as an artifact of the
`Trigger:package` job) and a Docker image (in the `Trigger:gitlab-docker`job)
which includes the changes from the MR. This package/docker image can be used to
deploy a GitLab instance locally and test the changes.

This job is automatically started if the MR includes changes to any of the
dependencies of the project, which has the potential to break builds in any of
the OSs GitLab provides packages for. For other MRs, this is available as a
manual job for the developers to play when needed.

## Contributing

Please see the [contribution guidelines](CONTRIBUTING.md)
